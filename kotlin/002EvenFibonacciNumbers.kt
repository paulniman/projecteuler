
/**
 * Returns a list of all fibonacci numbers less than [maxNum]
 */
fun getFibNums(maxNum: Int): List<Int> {
    var fibs: MutableList<Int> = mutableListOf(1, 2)
    while (fibs.takeLast(2).sum() < maxNum) {
        fibs.add(fibs.takeLast(2).sum())
    }
    return fibs
}

fun main(args: Array<String>) {
    val upTo = args[0].toInt()
    var total = 0
    for (n in getFibNums(upTo)) {
        if (n % 2 == 0) {
            total += n
        }
    }
    println(total)
}
