/*
 * Starting in the top left corner of a 2×2 grid, and only being able to move
 * to the right and down, there are exactly 6 routes to the bottom right corner.
 *
 * How many such routes are there through a 20×20 grid?
*/

/**
 * Factorial function for Longs
 */
fun Long.factorial(): Long {
    var f: Long = 1L
    for (i in 1..this) {
        f *= i
    }
    return f
}

/**
 * Raises a Long to [exp]. Math library only supports Floats and Doubles
 */
fun Long.pow(exp: Int): Long {
    var f: Long = this
    for (i in 2..exp) {
        f *= this
    }
    return f
}

fun main() {
    var pathCount: Long = 0L
    for (i in 0L..20L) {
        pathCount += (20L.factorial() / ((20L - i).factorial()*i.factorial())).pow(2)
    }
    println(pathCount)
}
