/*
 * The following iterative sequence is defined for the set of positive integers:
 *
 * n → n/2 (n is even)
 * n → 3n + 1 (n is odd)
 *
 * Using the rule above and starting with 13, we generate the following sequence:
 *
 * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 *
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains
 * 10 terms. Although it has not been proved yet (Collatz Problem), it is
 * thought that all starting numbers finish at 1.
 *
 * Which starting number, under one million, produces the longest chain?
 *
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */

val MAX_N = 1000000
var VALID_NUMS = IntArray(MAX_N) {1} // Array of 1s to mark valid starting points

/**
 * Returns the Collatz Sequence starting at [n] as a List
 */
fun collatzChain(n: Long): MutableList<Long> {
    var chain: MutableList<Long> = mutableListOf(n)
    while (chain.last() != 1L) {
        if (chain.last() % 2 == 0L) {
            chain.add(chain.last() / 2) // Even numbers
        } else {
            chain.add(chain.last() * 3 + 1) // Odd numbers
        }
    }
    return chain
}

fun main() {
    var maxLen = 0
    var maxLenIdx = 0
    var chain: MutableList<Long>
    for (i in 1..MAX_N-1) {
        // Skip numbers that have already been in a chain
        if (VALID_NUMS[i] == 0) continue

        // Calculate the chain and check its length
        chain = collatzChain(i.toLong())
        if (chain.size > maxLen) {
            maxLen = chain.size
            maxLenIdx = i
        }

        // Mark all numbers in the current chain as invalid
        for (c in chain) {
            if (c < MAX_N) VALID_NUMS[c.toInt()] = 0
        }
    }
    println("Max chain length: $maxLen at idx $maxLenIdx")
}
