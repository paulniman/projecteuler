import kotlin.math.sqrt

/**
 * Returns the number of factors of [n]
 */
fun getNumFactors(n: Int): Int {
    if (n == 1) return 1

    var factorCount = 2 // Count 1 and n
    for (f in 2..sqrt(n.toDouble()).toInt()) {
        if (n % f == 0) {
            if (n/f == f) {
                factorCount += 1 // If we hit the square only add 1 factor
            } else {
                factorCount += 2 // Otherwise add 2 for the pair
            }
        }
    }
    return factorCount
}

fun main(args: Array<String>) {
    var n = 0
    var i = 1
    while (true) {
        n += i++
        if (getNumFactors(n) >= 500) {
            println(n)
            break
        }
    }
}
