
fun main(args: Array<String>) {
    val upTo = args[0].toInt()
    var total = 0
    for (i in 1..(upTo-1)) {
        if (i % 3 == 0 || i % 5 == 0) {
            total += i
        }
    }
    println(total)
}

