/*
* @Author: Paul Niman
* @Date:   March 14, 2020 - 14:29:38
* @Last Modified by:   Paul Niman
* @Last Modified time: March 14, 2020 - 15:07:46
*/

package main

import (
    "fmt"
    "strings"
)

func intToName(x int) string {
    var s strings.Builder
    // Hundreds digit naming
    if x / 1000 > 0 {
        s.WriteString(intToName(x / 1000))
        s.WriteString("thousand")
        x %= 1000
    }
    // Hundreds digit naming
    if x / 100 > 0 {
        s.WriteString(intToName(x / 100))
        s.WriteString("hundred")
        x %= 100
        if x > 0 {
            s.WriteString("and")
        }
    }
    // Tens digit naming
    if x / 10 > 1 {
        if x / 10 == 9 {
            s.WriteString("ninety")
        } else if x / 10 == 8 {
            s.WriteString("eighty")
        } else if x / 10 == 7 {
            s.WriteString("seventy")
        } else if x / 10 == 6 {
            s.WriteString("sixty")
        } else if x / 10 == 5 {
            s.WriteString("fifty")
        } else if x / 10 == 4 {
            s.WriteString("forty")
        } else if x / 10 == 3 {
            s.WriteString("thirty")
        } else if x / 10 == 2 {
            s.WriteString("twenty")
        }
        x %= 10
    } else if x / 10 == 1 {
        if x == 19 {
            s.WriteString("nineteen")
        } else if x == 18 {
            s.WriteString("eighteen")
        } else if x == 17 {
            s.WriteString("seventeen")
        } else if x == 16 {
            s.WriteString("sixteen")
        } else if x == 15 {
            s.WriteString("fifteen")
        } else if x == 14 {
            s.WriteString("fourteen")
        } else if x == 13 {
            s.WriteString("thirteen")
        } else if x == 12 {
            s.WriteString("twelve")
        } else if x == 11 {
            s.WriteString("eleven")
        } else if x == 10 {
            s.WriteString("ten")
        }
        x = 0
    }
    // Ones digit naming
    if x == 9 {
        s.WriteString("nine")
    } else if x == 8 {
        s.WriteString("eight")
    } else if x == 7 {
        s.WriteString("seven")
    } else if x == 6 {
        s.WriteString("six")
    } else if x == 5 {
        s.WriteString("five")
    } else if x == 4 {
        s.WriteString("four")
    } else if x == 3 {
        s.WriteString("three")
    } else if x == 2 {
        s.WriteString("two")
    } else if x == 1 {
        s.WriteString("one")
    }

    return s.String()
}

func main() {
    letterCount := 0
    for i := 0; i <= 1000; i++ {
        letterCount += len(intToName(i))
    }
    fmt.Println(letterCount)
}
