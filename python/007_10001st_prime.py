#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   April 04, 2019 - 19:30:03
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

What is the 10 001st prime number?
"""

import math

import utils
from utils import measure_time

@measure_time(1)
def find_nth_prime(n):
    """Finds the nth prime number.

    First approximates the number it needs to check up to using the Prime Number
    Theorem * 1.2, then finds primes using the Sieve or Eratosthenes.

    Args:
        n (int): nth Prime to find
    """
    max_n_to_check = math.ceil(n * math.log(n) * 1.2)
    primes = utils.find_primes(max_n_to_check)
    return primes[n-1]


if __name__ == '__main__':
    print(find_nth_prime(10001))
