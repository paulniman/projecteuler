#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   October 08, 2020 - 19:48:57
# @Last Modified: October 08, 2020 - 20:26:29
#
################################################################################

"""
You are given the following information, but you may prefer to do some research
for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.

A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible
by 400. How many Sundays fell on the first of the month during the twentieth century
(1 Jan 1901 to 31 Dec 2000)?
"""

from utils import measure_time

#                   Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
MONTH_DAY_COUNTS = (31,  28,  31,  30,  31,  30,  31,  31,  30,  31,  30,  31)

def is_sunday(day_number):
    """Returns True if the given day number (starting at Jan 1, 1901) is a Sunday
    """
    return (day_number % 7) == 5


def is_leap_year(year):
    """Checks if the given year between 1901 and 2000 is a leap year

    Args:
        year (int): Year between 1901 and 2000

    Returns:
        bool: True if the year is a leap year, False otherwise
    """
    return (year % 4) == 0


def month_firsts():
    """Generates a list of day numbers that are the first of a month.

    Yields:
        int: Day number (0 = Jan 1, 1901) that is the first of a month
    """
    cur_first = 0 # Tracks the current "first day of the month"
    for year in range(1901, 2001):
        for month_len in MONTH_DAY_COUNTS:
            yield cur_first

            cur_first += month_len
            if month_len == 28 and year % 4 == 0:
                cur_first += 1 # Account for leap year extra day


@measure_time(1)
def main():
    sunday_firsts = 0
    for d in month_firsts():
        if is_sunday(d):
            sunday_firsts += 1

    print(sunday_firsts)


if __name__ == '__main__':
    main()
