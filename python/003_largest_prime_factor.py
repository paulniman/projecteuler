#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 23, 2019 - 12:22:03
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

import math

import numpy as np

import utils
from utils import measure_time


@measure_time(1)
def largest_prime_factor(n):
    """Finds the largest prime factor of the given number

    Args:
        n (int): Nubmer to find the largest prime factor of

    Returns:
        int: Largest prime factor
    """
    primes = utils.find_primes(math.ceil(math.sqrt(n)))
    # Work backwards starting at the largest factor until we find a prime
    for f in reversed(primes):
        if n % f == 0:
            return f


if __name__ == '__main__':
    print(largest_prime_factor(600851475143))
