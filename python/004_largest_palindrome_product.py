#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 23, 2019 - 12:51:45
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
A palindromic number reads the same both ways. The largest palindrome made from
the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

from utils import measure_time

def is_palindrome(n):
    """Checks whether the argument is a palindrome

    Args:
        n (int, float, str): Potential palindrome

    Returns:
        bool: Whether the number is a palindrome
    """
    return str(n) == str(n)[::-1]


@measure_time(1)
def largest_palindrome_product(max_n):
    """Finds the largest palindrome product of 2 numbers less than max_n

    Args:
        max_n (int): Max multiplier

    Returns:
        int: Largest palindrome
    """
    largest = 0
    for i in range(max_n):
        for j in range(max_n):
            prod = i * j
            if is_palindrome(prod) and prod > largest:
                largest = prod

    return largest


if __name__ == '__main__':
    print(largest_palindrome_product(999))
