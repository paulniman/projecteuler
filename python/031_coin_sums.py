#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 20, 2019 - 23:28:20
# @Last Modified: April 12, 2019 - 23:24:45
#
################################################################################

"""
In England the currency is made up of pound, £, and pence, p, and there are
eight coins in general circulation:

    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).

It is possible to make £2 in the following way:

    1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p

How many different ways can £2 be made using any number of coins?
"""

from utils import measure_time

# Global counter
total = 0

def coin_sums(amount, coin_list):
    if amount == 0:
        global total
        total += 1

    for i, coin in enumerate(coin_list):
        if amount >= coin:
            coin_sums(amount-coin, coin_list[i:])


@measure_time(1)
def main():
    global total
    total = 0
    coin_sums(200, [200, 100, 50, 20, 10, 5, 2, 1])

if __name__ == '__main__':
    main()
    print(total)
