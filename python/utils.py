#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   April 04, 2019 - 19:30:48
# @Last Modified: April 13, 2019 - 18:50:31
#
################################################################################

"""
A file for common functions that multiple problems may use.
"""

import math
import time
from functools import wraps

import numpy as np


def measure_time(iters=1):
    """Decorator for running a function X times and printing the fastest time

    Args:
        iters (int, optional): Number of iterations
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            print(f'Running {func.__name__}() {iters} times')
            min_runtime = 999999
            for i in range(iters):
                start_t = time.time()
                result = func(*args, **kwargs)
                runtime = time.time() - start_t
                if runtime < min_runtime:
                    min_runtime = runtime

            print(f'Best time: {min_runtime:0.3f} seconds')
            return result

        return wraps(func)(wrapper)

    return decorator


def find_primes(up_to):
    """Returns a list of all primes up to the given number

    Args:
        up_to (int): Largest number to include
    """
    primes = np.ones(up_to)
    # Mark 0 and 1 as not prime
    primes[0] = primes[1] = 0
    for i in range(2, int(math.sqrt(up_to))):
        # No need to mark multiples of composites
        if not primes[i]:
            continue

        for j in range(2, math.ceil(up_to / i)):
            # Mark each multiple as not prime
            primes[i*j] = 0

    return [i for i in range(up_to) if primes[i]]


def prod(x):
    """Returns the product of all elements in the list.

    Can't use numpy.prod because it overflows

    Args:
        x (list): List of numbers
    """
    prod = 1
    for i in x:
        prod *= i

    return prod
