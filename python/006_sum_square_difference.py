#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 26, 2019 - 22:36:01
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
The sum of the squares of the first ten natural numbers is,

    1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is,

    (1 + 2 + ... + 10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural
numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred
natural numbers and the square of the sum.
"""

from utils import measure_time

def sum_squares_up_to(n):
    """Sums the squares of each number up to (and including) n

    Args:
        n (int): Max number to square and sum

    Returns:
        int: Sum of squares
    """
    return sum([x**2 for x in range(n+1)])


def square_sums_up_to(n):
    """Squares the sum of all numbers up to (and including) n

    Args:
        n (int): Max number to sum before squaring

    Returns:
        int: Square of sums
    """
    return ((n + 1) * n // 2) ** 2


@measure_time(1)
def sum_square_difference(n):
    """Returns the difference between the sum of squares and square of sums

    Args:
        n (int): Number to go up to in calculations

    Returns:
        int: Difference
    """
    return square_sums_up_to(n) - sum_squares_up_to(n)


if __name__ == '__main__':
    print(sum_square_difference(100))
