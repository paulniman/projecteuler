#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 26, 2019 - 21:49:40
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
2520 is the smallest number that can be divided by each of the numbers from 1
to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20?
"""

import math

from utils import measure_time


def get_prime_factors(n):
    """Returns a list of all prime factors of the given number

    Args:
        n (int): Integer to get factors of
    """
    factors = []
    for i in range(2, int(math.sqrt(n))+1):
        while n % i == 0:
            factors.append(i)
            n //= i

    if n > 1:
        factors.append(n)

    return factors


@measure_time(1)
def smallest_multiple_up_to(n):
    """Finds the smallest number thats a multiple of all number up to n

    Starting at n and working downwards, finds all numbers that only have
    1 unique factor. This equates to powers and primes that aren't roots of
    already counted powers. Returns the product of these numbers.

    Args:
        n (int): Max factor

    Returns:
        int: Smallest multiple
    """
    smallest_multiple = 1
    for i in range(n, 1, -1):
        prime_facs = get_prime_factors(i)
        # Append factors from numbers who only have 1 unique prime factor
        if len(set(prime_facs)) == 1:
            if smallest_multiple % i != 0:
                smallest_multiple *= i

    return smallest_multiple

if __name__ == '__main__':
    print(smallest_multiple_up_to(20))
