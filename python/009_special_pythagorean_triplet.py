#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   April 06, 2019 - 23:16:51
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a^2 + b^2 = c^2
For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""

import math

from utils import measure_time


@measure_time(1)
def find_triplets(s):
    """Finds the triplets of Pythagorean integers that add up to s

    Args:
        s (int): Sums of the triplet(s) to find

    Returns:
        list: List of triplets in tuple form
    """
    for a in range(1, s // 3):
        for b in range(a, s):
            c = math.sqrt(a**2 + b**2)
            if c.is_integer() and (a + b + int(c)) == s:
                return (a, b, int(c))

    # Mo match was found
    return None

if __name__ == '__main__':
    a, b, c = find_triplets(1000)
    print(a, b, c, a * b * c)
