#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 20, 2019 - 21:44:09
# @Last Modified: April 12, 2019 - 23:23:18
#
################################################################################

"""
If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""

from utils import measure_time

def sum_all_up_to(n):
    """Sums all positive integers less than or equal to n.

    e.g. 1 + 2 + 3 + 4 + ... + n

    Args:
        n (int): Number to sum integers up to

    Returns:
        int: Sum
    """
    return (n + 1) * n // 2


def sum_multiples(factor, less_than):
    """Sums all multiples of the given factor less than the provided max

    Args:
        factor (int): Factor to sum multiples of
        less_than (int): Integer to stop at
    """
    num_mults = (less_than - 1) // factor
    return sum_all_up_to(num_mults) * factor


@measure_time(1)
def main():
    total = sum_multiples(3, 1000) # Add all multples of 3
    total += sum_multiples(5, 1000) # Add all multiples of 5
    total -= sum_multiples(15, 1000) # Subtract numbers counted twice
    return total


if __name__ == '__main__':
    print(main())
