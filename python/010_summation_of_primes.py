#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   April 12, 2019 - 23:31:22
# @Last Modified: April 13, 2019 - 19:00:00
#
################################################################################

"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""

from utils import measure_time, find_primes


@measure_time(1)
def sum_primes(n):
    """Sums all primes up to (not including) n

    Args:
        n (int): Number to sum primes up to

    Returns:
        int: Sum of primes
    """
    return sum(find_primes(n))


if __name__ == '__main__':
    print(sum_primes(2000000))
